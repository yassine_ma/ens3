# exemple fictif (cf. schéma précédent)
> ls -li /usr
     9 drwxr-xr-x   2 ...    512 Jul 14 01:00 bin
    10 drwxr-xr-x   2 ...    512 Jul 14 01:00 include
    11 drwxr-xr-x   2 ...    512 Jul 14 01:00 lib

# exemple réel
> ls -li /usr
262146 drwxr-xr-x   2 ... 110592 Jul 14 01:00 bin
262148 drwxr-xr-x  44 ...  20480 Apr 14 01:00 include
262149 drwxr-xr-x 143 ...  12288 Jul 14 01:00 lib
