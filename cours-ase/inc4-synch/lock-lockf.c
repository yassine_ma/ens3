void verrouiller (int fd) {
  if (lockf (fd, F_LOCK, 0) == -1)
    raler (...) ;
}

void deverrouiller (int fd) {
  if (lockf (fd, F_ULOCK, 0) == -1)
    raler (...) ;
}
