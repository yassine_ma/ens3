int ntops ;				// variable globale : nb de tops d'horloge écoulés
int commuter ;				// variable globale : il faut commuter (ou non)

void traiter_int_horloge (void)		// appelée à chaque interruption de l'horloge
{
  curproc->utilisation_cpu++ ;		// augmenter le temps du processus courant
  ntops++ ;
  if (ntops >= 50) {			// si 50 tops d'horloge, soit 1 sec
    ntops = 0 ;

    for (int i = 0 ; i < nproc ; i++)	// diviser par 2 toutes les priorités
      proc[i].utilisation_cpu /= 2 ;

    // priorité minimum = ma priorité pour démarrer
    int min_prio = curproc->utilisation_cpu + curproc->nice ;
    commuter = -1 ;			// pas besoin de commuter

    for (int i = 0 ; i < nproc ; i++) {	// calculer les priorités de tous les processus
      if (&proc[i] != curproc) {
	// calcul de la priorité du processus i
	int prio_i = proc[i].utilisation_cpu + proc[i].nice ;

	// prendre la priorité minimum
	if (prio_i < min_prio) {
	  commuter = i;			// il faut commuter pour l'élu n° i
	  min_prio = prio_i ;		// lors du prochain retour d'interruption/exception
	}
      }
    }
  }
}
